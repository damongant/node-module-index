var fs = require('fs')
  , __ = require('underscore')
  , path = require('path');

__.each(fs.readdirSync(__dirname), function(item) {
	var fullpath = path.join(__dirname, item);
	var isDirectory = fs.statSync(fullpath).isDirectory();
	if ((item.indexOf('.js', item.length - 3) == -1 && !isDirectory) || item == "index.js")
		return;
	if (!isDirectory)
		var module_name = item.substring(0, item.length - 3);
	else
		var module_name = item;
	exports[module_name] = require('./' + item);
})