node-module-index
=================
A simple reusable snippet for the default node.js package management system. This script will load all scripts and folders in the module directory populate the result to the <code>exports</code> array.

Requires underscore (<code>npm install underscore</code>)